package article

import (
	"context"

	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/gin-boilerplate/helper"
	"gitlab.com/mqdvi/gin-boilerplate/models"
)

type ArticleRepositoryInterface interface {
	GetByID(ctx context.Context, db *sqlx.DB, id int64) (*models.Article, error)
	GetArticlesPagination(ctx context.Context, db *sqlx.DB, filter *models.GetArticlesFilter, meta *helper.Meta) ([]models.Article, error)
	GetArticlesCount(ctx context.Context, db *sqlx.DB, filter *models.GetArticlesFilter, meta *helper.Meta) (uint64, error)
	Create(ctx context.Context, db *sqlx.DB, payload *models.CreateArticlePayload) (*models.Article, error)
}

type articleRepository struct {
	dbName string
}

var (
	sortMap = map[string]string{
		"id":         "articles.id ASC",
		"-id":        "articles.id DESC",
		"title":      "articles.title ASC",
		"-title":     "articles.title DESC",
		"name":       "authors.name ASC",
		"-name":      "authors.name DESC",
		"createdAt":  "articles.created_at ASC",
		"-createdAt": "articles.created_at DESC",
		"updatedAt":  "articles.updated_at ASC",
		"-updatedAt": "articles.updated_at DESC",
	}
)

func NewRepository(dbName string) ArticleRepositoryInterface {
	return &articleRepository{
		dbName: dbName,
	}
}

func (repo *articleRepository) selectFields() []string {
	return []string{
		"articles.id",
		"articles.title",
		"articles.body",
		"authors.name",
		"articles.created_at",
		"articles.updated_at",
	}
}

func (repo *articleRepository) GetTableName() string {
	return "articles"
}

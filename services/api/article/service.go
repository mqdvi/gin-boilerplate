package article

import (
	"context"

	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/gin-boilerplate/helper"
	"gitlab.com/mqdvi/gin-boilerplate/models"
	"gitlab.com/mqdvi/gin-boilerplate/services/api/author"
)

type ArticleServiceInterface interface {
	GetArticlesPagination(ctx context.Context, filter *models.GetArticlesFilter, meta *helper.Meta) (*helper.PaginationResponse, error)
	Create(ctx context.Context, payload *models.CreateArticlePayload) (*models.Article, error)
}

type articleService struct {
	db         *sqlx.DB
	repo       ArticleRepositoryInterface
	authorRepo author.AuthorRepositoryInterface
}

func NewService(db *sqlx.DB, repo ArticleRepositoryInterface, authorRepo author.AuthorRepositoryInterface) ArticleServiceInterface {
	return &articleService{
		db:         db,
		repo:       repo,
		authorRepo: authorRepo,
	}
}

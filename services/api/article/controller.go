package article

import "github.com/go-playground/validator/v10"

type articleController struct {
	articleSvc ArticleServiceInterface
	validator  *validator.Validate
}

func NewController(articleSvc ArticleServiceInterface) *articleController {
	return &articleController{articleSvc: articleSvc}
}

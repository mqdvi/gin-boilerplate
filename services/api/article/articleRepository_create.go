package article

import (
	"context"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/gin-boilerplate/models"
)

func (repo *articleRepository) Create(ctx context.Context, db *sqlx.DB, payload *models.CreateArticlePayload) (*models.Article, error) {
	query, args, _ := sq.Insert(repo.GetTableName()).
		SetMap(sq.Eq{
			"author_id":  payload.AuthorID,
			"title":      payload.Title,
			"body":       payload.Body,
			"created_at": sq.Expr("NOW()"),
			"updated_at": sq.Expr("NOW()"),
		}).ToSql()

	result, err := db.ExecContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	return repo.GetByID(ctx, db, id)
}

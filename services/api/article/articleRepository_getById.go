package article

import (
	"context"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/gin-boilerplate/models"
)

func (repo *articleRepository) GetByID(ctx context.Context, db *sqlx.DB, id int64) (*models.Article, error) {
	var result models.Article

	query, args, _ := sq.Select(repo.selectFields()...).
		From(repo.GetTableName()).
		Join("authors ON articles.author_id = authors.id").
		Limit(1).
		ToSql()

	err := db.GetContext(ctx, &result, query, args...)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

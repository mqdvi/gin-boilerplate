package article

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/mqdvi/gin-boilerplate/helper"
	"gitlab.com/mqdvi/gin-boilerplate/models"
	"gitlab.com/mqdvi/gin-boilerplate/services/config"
)

func (ctrl *articleController) HandlerGetArticlesPagination(c *gin.Context) {
	var limit uint64
	limitStr := c.Query("itemsPerPage")

	limitInt := helper.StringToUint64(limitStr)
	if limitInt > 0 {
		limit = limitInt
	} else {
		limit = config.GetConfig().DefaultPaginationLimit
	}

	paging := helper.NewMeta(limit)
	paging.FromContext(c)

	filter := models.GetArticlesFilter{}
	filter.FromContext(c)

	result, err := ctrl.articleSvc.GetArticlesPagination(c, &filter, paging)
	if err != nil {
		errorResponse := helper.NewErrorResponse("articles-500", err.Error())

		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}

	c.JSON(http.StatusOK, result)
}

package article

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/mqdvi/gin-boilerplate/helper"
	"gitlab.com/mqdvi/gin-boilerplate/models"
)

func (ctrl *articleController) HandlerCreateArticle(c *gin.Context) {
	payload := new(models.CreateArticlePayload)
	if err := c.ShouldBindJSON(&payload); err != nil {
		errorResponse := helper.NewErrorResponse("articles-400", err.Error())
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}
	if err := ctrl.validator.Struct(payload); err != nil {
		errorResponse := helper.NewErrorResponse("articles-400", err.Error())
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}

	result, err := ctrl.articleSvc.Create(c, payload)
	if err != nil {
		errorResponse := helper.NewErrorResponse("articles-500", err.Error())
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}

	response := helper.JsonResponse{
		Data: result,
	}

	c.JSON(http.StatusCreated, response)
}

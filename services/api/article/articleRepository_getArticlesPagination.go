package article

import (
	"context"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/gin-boilerplate/helper"
	"gitlab.com/mqdvi/gin-boilerplate/models"
)

func (repo *articleRepository) GetArticlesPagination(ctx context.Context, db *sqlx.DB, filter *models.GetArticlesFilter, meta *helper.Meta) ([]models.Article, error) {
	var result []models.Article

	builder := sq.Select(repo.selectFields()...).
		From(repo.GetTableName()).
		Join("authors ON articles.author_id = authors.id")

	query, args, _ := repo.appendGetArticlesQuery(builder, filter).
		Limit(meta.Limit).
		Offset(meta.Offset).
		ToSql()

	err := db.SelectContext(ctx, &result, query, args...)
	if err != nil {
		helper.Logger.Error().
			Strs("tags", []string{"ArticleRepository", "GetArticlesPagination"}).
			Str("query", query).
			Msg(err.Error())
		return result, err
	}

	return result, nil
}

func (repo *articleRepository) GetArticlesCount(ctx context.Context, db *sqlx.DB, filter *models.GetArticlesFilter, meta *helper.Meta) (uint64, error) {
	var result uint64

	builder := sq.Select("COUNT(articles.id)").
		From(repo.GetTableName()).
		Join("authors ON articles.author_id = authors.id")

	query, args, _ := repo.appendGetArticlesQuery(builder, filter).ToSql()
	helper.Logger.Debug().
		Strs("tags", []string{"ArticleRepository", "GetArticlesCount"}).
		Str("query", query)

	err := db.GetContext(ctx, &result, query, args...)
	if err != nil {
		helper.Logger.Error().
			Strs("tags", []string{"ArticleRepository", "GetArticlesCount"}).
			Str("query", query).
			Msg(err.Error())
		return result, err
	}

	return result, nil
}

func (repo *articleRepository) appendGetArticlesQuery(builder sq.SelectBuilder, filter *models.GetArticlesFilter) sq.SelectBuilder {
	if filter.Search != "" {
		builder = builder.Where(sq.Or{
			sq.Like{"articles.title": "%" + filter.Search + "%"},
			sq.Like{"authors.name": "%" + filter.Search + "%"},
		})
	}

	sortBy := sortMap[filter.Sort]
	if sortBy == "" {
		sortBy = "articles.created_at DESC"
	}
	builder = builder.OrderBy(sortBy)

	return builder
}

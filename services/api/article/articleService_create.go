package article

import (
	"context"

	"github.com/microcosm-cc/bluemonday"

	"gitlab.com/mqdvi/gin-boilerplate/models"
)

func (svc *articleService) Create(ctx context.Context, payload *models.CreateArticlePayload) (*models.Article, error) {
	_, err := svc.authorRepo.GetAuthorByID(ctx, svc.db, payload.AuthorID)
	if err != nil {
		return nil, err
	}

	svc.sanitizePayload(payload)

	result, err := svc.repo.Create(ctx, svc.db, payload)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (svc *articleService) sanitizePayload(payload *models.CreateArticlePayload) {
	p := bluemonday.StrictPolicy()

	payload.Body = p.Sanitize(payload.Body)
	payload.Title = p.Sanitize(payload.Title)
}

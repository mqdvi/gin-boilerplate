package article

import (
	"context"

	"gitlab.com/mqdvi/gin-boilerplate/helper"
	"gitlab.com/mqdvi/gin-boilerplate/models"
)

func (svc *articleService) GetArticlesPagination(ctx context.Context, filter *models.GetArticlesFilter, meta *helper.Meta) (*helper.PaginationResponse, error) {
	count, err := svc.repo.GetArticlesCount(ctx, svc.db, filter, meta)
	if err != nil {
		helper.Logger.Error().
			Strs("tags", []string{"ArticleRepository", "GetArticlesCount"}).
			Msg(err.Error())
		return nil, err
	}
	meta.Count = uint64(count)

	articles, err := svc.repo.GetArticlesPagination(ctx, svc.db, filter, meta)
	if err != nil {
		helper.Logger.Error().
			Strs("tags", []string{"ArticleRepository", "GetArticlesPagination"}).
			Msg(err.Error())
		return nil, err
	}

	data := helper.Data{}
	data = data.WithMeta(meta)
	data = data.WithItems(articles)
	resp := helper.PaginationResponse{
		Data: data,
	}

	return &resp, nil
}

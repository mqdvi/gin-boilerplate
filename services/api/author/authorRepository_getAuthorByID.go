package author

import (
	"context"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/gin-boilerplate/models"
)

func (repo *authorRepository) GetAuthorByID(ctx context.Context, db *sqlx.DB, id int64) (*models.Author, error) {
	query, args, _ := sq.Select(repo.selectFields()...).
		From(repo.GetTableName()).
		Where(sq.Eq{"id": id}).
		Limit(1).
		ToSql()

	var result models.Author
	err := db.GetContext(ctx, &result, query, args...)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

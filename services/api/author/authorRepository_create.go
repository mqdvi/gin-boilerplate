package author

import (
	"context"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/gin-boilerplate/models"
)

func (repo *authorRepository) Create(ctx context.Context, db *sqlx.DB, name string) (*models.Author, error) {
	query, args, _ := sq.Insert(repo.GetTableName()).
		SetMap(sq.Eq{"name": name}).
		ToSql()

	result, err := db.ExecContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	return &models.Author{
		ID:   id,
		Name: name,
	}, nil
}

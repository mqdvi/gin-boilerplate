package author

import (
	"context"

	"github.com/jmoiron/sqlx"

	"gitlab.com/mqdvi/gin-boilerplate/models"
)

type AuthorRepositoryInterface interface {
	GetTableName() string
	GetAuthorByID(ctx context.Context, db *sqlx.DB, id int64) (*models.Author, error)
	GetAuthorByName(ctx context.Context, db *sqlx.DB, name string) (*models.Author, error)
	Create(ctx context.Context, db *sqlx.DB, name string) (*models.Author, error)
}

type authorRepository struct {
	dbName string
}

func NewRepository(dbName string) AuthorRepositoryInterface {
	return &authorRepository{
		dbName: dbName,
	}
}

func (repo *authorRepository) selectFields() []string {
	return []string{
		"id",
		"name",
	}
}

func (repo *authorRepository) GetTableName() string {
	return "authors"
}

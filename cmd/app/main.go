package main

import (
	"gitlab.com/mqdvi/gin-boilerplate/services/config"
	"gitlab.com/mqdvi/gin-boilerplate/services/application"
)

func main() {
	conf := config.GetConfig()

	app := application.NewApp(&conf)
	app.Start()
	defer app.PreStop()
}

package models

import (
	"time"

	"github.com/gin-gonic/gin"
)

type Article struct {
	ID         int64     `db:"id" json:"id"`
	AuthorName string    `db:"name" json:"authorName"`
	Title      string    `db:"title" json:"title"`
	Body       string    `db:"body" json:"body"`
	CreatedAt  time.Time `db:"created_at" json:"createdAt"`
	UpdatedAt  time.Time `db:"updated_at" json:"updatedAt"`
}

type CreateArticlePayload struct {
	AuthorID int64  `json:"authorId" validate:"required"`
	Title    string `json:"title" validate:"required"`
	Body     string `json:"body" validate:"required"`
}

type GetArticlesFilter struct {
	Search string
	Sort   string
}

func (m *GetArticlesFilter) FromContext(c *gin.Context) *GetArticlesFilter {
	m.Search = c.Query("search")
	m.Sort = c.Query("sort")

	return m
}
